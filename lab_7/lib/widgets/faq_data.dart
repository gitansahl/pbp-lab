import 'package:flutter/material.dart';

import '../models/question.dart';

const DUMMY_FAQ = const [
  Question(
    question: "Apakah Gitan Ganteng?",
    answer: "Sangat ganteng ganteng ganteng"
  ),
  Question(
      question: "Apakah Lab SDA Susah",
      answer: "Sangat susah susah susah"),
];