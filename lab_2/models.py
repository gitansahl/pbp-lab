from django.db import models

# Create your models here.

class Note(models.Model):
    to = models.CharField(max_length=30)
    from_field = models.CharField("From", max_length=30)
    title = models.CharField(null = True, max_length=30)
    message = models.TextField(null = True)