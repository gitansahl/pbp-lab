from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers
# Create your views here.
def index(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab2.html', response)

def xml(response):
    note = Note.objects.all()
    data = serializers.serialize('xml',note)
    return HttpResponse(data, content_type="application/xml")

def json(response):
    note = Note.objects.all()
    data = serializers.serialize('json',note)
    return HttpResponse(data, content_type="application/json")