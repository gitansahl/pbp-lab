import 'package:flutter/cupertino.dart';

class Question {
  final String question, answer;

  const Question({
    @required this.question,
    this.answer,
});
}