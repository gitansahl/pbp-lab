import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/widgets/main_drawer.dart';

import '../models/question.dart';
import '../widgets/faq_data.dart';

class FaqScreen extends StatelessWidget {
  static const routeName ='faq/';

  @override
  Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(title: Text('FAQ Project-Channel'),),
        body: ListView.builder(
            itemBuilder: (BuildContext context, int index) {
              return new StuffInTiles(listOfTiles[index]);
            },
        itemCount: listOfTiles.length,),
        drawer: MainDrawer(),
      );
  }
}

class StuffInTiles extends StatelessWidget {
  final MyTile myTile;
  StuffInTiles(this.myTile);

  @override
  Widget build(BuildContext context) {
    return _buildTiles(myTile);
  }

  Widget _buildTiles(MyTile t) {
    if (t.children.isEmpty)
      return new ListTile(
          dense: true,
          enabled: true,
          isThreeLine: false,
          onLongPress: () => print("long press"),
          onTap: () => print("tap"),
          title: new Text(t.title, style: TextStyle(fontSize: 16.0)));

    return new ExpansionTile(
      key: new PageStorageKey<int>(3),
      backgroundColor: Colors.white70,
      collapsedBackgroundColor: Colors.white70,
      collapsedTextColor: Color(0xff374ABE),
      title: new Text(t.title, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0)),
      children: t.children.map(_buildTiles).toList(),
    );
  }
}

class MyTile {
  String title;
  List<MyTile> children;
  MyTile(this.title, [this.children = const <MyTile>[]]);
}

List<MyTile> listOfTiles = <MyTile> [
  for(var question in DUMMY_FAQ)
    new MyTile(question.question ,
    <MyTile>[
      new MyTile(question.answer)
    ])
];
