from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.http.response import HttpResponseRedirect
from django.http import response
from django.shortcuts import render

# Create your views here.
def index(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab4_index.html', response)
def add_note(request):
    form = NoteForm(request.POST or None)
    if form.is_valid() and request.method=="POST":
        form.save()
        return HttpResponseRedirect('/lab-4')
    else:
        return render(request, 'lab4_form.html', {'form':form})
def note_list(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)