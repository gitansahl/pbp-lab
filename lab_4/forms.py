from lab_2.models import Note
from django import forms

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'
        labels = {
            'to' : '',
            'from_field' : '',
            'title' : '',
            'message' : '',
        }
        widgets = {
            'to' : forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Penerima'}),
            'from_field' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Pengirim'}),
            'title' :forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Judul'}),
            'message' :forms.Textarea(attrs={'class': 'form-control','placeholder': 'Pesan'})
        }

